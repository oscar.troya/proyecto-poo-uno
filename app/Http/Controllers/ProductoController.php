<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Producto;
class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function TallerCorteDos()
    {
        $TipoMotor = 3;


        switch ($TipoMotor) {
    
        case '0':
        echo("no hay establecido un valor definido para el tipo bomba");
        break;
        case '1':
        echo("La bomba es una bomba de agua");
        break;
        case '2':
        echo("La bomba es una bomba de gasolina");
        break;
        case '3':
        echo("La bomba es una bomba de hormigon");
        break;
        case '4':
        echo("La bomba es una bomba de pasta alimenticia");
        break;
        default:
        echo("No existe un valor valido para el tipo de bomba");
        break;
}
    }
    
    
    
    
     public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
