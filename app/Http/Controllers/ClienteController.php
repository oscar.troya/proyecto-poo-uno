<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InicioCliente(Request $request)
    {
        //dd('hola mundo');
        $cliente = Cliente::all();
        //d($Cliente);
        return view('Clientes.inicio')->with('Cliente',$cliente);

    }

    public function CrearCliente(Request $request){
                $cliente = Cliente::all();
                return view('Clientes.crear')->with('Cliente',$cliente);

    }

    public function GuardarCliente(Request $request){

        $this->validate($request, [
            'nombre'=>'required', 
            'apellido'=>'required', 
            'cedula'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'fecha_nacimiento'=>'required',
            'email'=>'required'
        ]);
        $cliente = new Cliente;
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->cedula= $request->cedula;
        $cliente->direccion = $request->direccion;
        $cliente->telefono= $request->telefono;
        $cliente->fecha_nacimiento = $request->fecha_nacimiento;
        $cliente->email = $request->email;
        $cliente->save();
        return redirect()->route('list.clientes');

    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
