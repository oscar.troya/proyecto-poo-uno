<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

route::group(['middleware' => ['auth']],function(){

    route::post('/guardar/clientes',['as'=>'guardar.clientes', 'uses'=> 'ClienteController@GuardarCliente']);
    route::get('/lista/clientes',['as'=>'list.clientes', 'uses'=> 'ClienteController@InicioCliente']);
    route::get('/crear/clientes',['as'=>'crear.clientes', 'uses'=> 'ClienteController@CrearCliente']);
});


