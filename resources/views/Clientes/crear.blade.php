@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR CLIENTES</div>
                    <div class ="col text-right">
                        <a href="{{route('list.clientes')}}"class="btn btn-sm btn-primary">Cancelar</a>
                    </div>
                    <div class="card-body">

                        <form role="form" method="post" action="{{route('guardar.clientes')}}">
                            
                            {{ csrf_field() }}
                            {{ method_field("post") }}
                            
                            <div class="row">
                                <div class="col-lg-4">
                                <label class="from-control-label" for="nombre" >Nombre Cliente</label>
                                <input type="text" class="from-control" name="nombre"> 
                            </div>


                            
                            <div class="col-lg-4">
                                <label class="from-control-label" for="apellido" >Apellido</label>
                                <input type="text" class="from-control" name="apellido"> 
                            </div>


                            
                            <div class="col-lg-4">
                                <label class="from-control-label" for="cedula" >Cedula</label>
                                <input type="number" class="from-control" name="cedula"> 
                                
                            </div>


                            
                            <div class="col-lg-4">
                                <label class="from-control-label" for="direccion" >Direccion</label>
                                <input type="varchar" class="from-control" name="direccion"> 
                                
                            </div>


                            
                            <div class="col-lg-4">
                                <label class="from-control-label" for="telefono" >Telefono</label>
                                <input type="number" class="from-control" name="telefono"> 
                                
                            </div>


                            
                            <div class="col-lg-4">
                                <label class="from-control-label" for="fecha_nacimiento" >Fecha_Nacimiento</label>
                                <input type="date" class="from-control" name="fecha_nacimiento"> 
                                
                            </div>


                            <div class="col-lg-4">
                                <label class="from-control-label" for="email" >Email</label>
                                <input type="varchar" class="from-control" name="email"> 
                                
                            </div>
                            


                            <button type="submit" class="btn btn-success pull-rigth">Guardar</button>
                            
                        </form>     
                    </div>
                    
                    



                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection